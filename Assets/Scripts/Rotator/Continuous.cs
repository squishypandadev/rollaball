﻿using UnityEngine;

namespace RollABall {
  namespace Rotator {
    [AddComponentMenu("Roll A Ball/Continuos Rotator")]
    public class Continuous : MonoBehaviour
    {
      [SerializeField]
      private Vector3 _rotation = Vector3.zero;

      private void Update() {
        this.transform.Rotate(this._rotation * Time.deltaTime);
      }
    }
  }
}
