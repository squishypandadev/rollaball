﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace RollABall {
  namespace Rotator {
    [AddComponentMenu("Roll A Ball/Player Input Rotator")]
    public class PlayerInput : MonoBehaviour
    {
      [SerializeField]
      private _RotationValues _rotationValues =
        new _RotationValues();


      private Vector2 _movementVect = new Vector2(
        0.0f,
        0.0f
      );

      private Vector2 _rotationVect;

      private Vector2 _RotationVect {
        get => this._rotationVect;
        set {
          this._rotationVect = new Vector2(
            this._Clamp(value.x),
            this._Clamp(value.y)
          );
        }
      }

      private void Start() {
        this._rotationVect = new Vector2(
          this.transform.rotation.x,
          this.transform.rotation.y
        );
      }

      private void Update() {
        this._RotationVect +=
          this._movementVect * this._rotationValues.Scalar;

        this.transform.eulerAngles = new Vector3(
          this._RotationVect.x,
          0.0f,
          this._RotationVect.y
        );
      }

      private void OnMoveX(InputValue inputValue) {
        this._movementVect.x = inputValue.Get<float>();
      }

      private void OnMoveZ(InputValue inputValue) {
        this._movementVect.y = inputValue.Get<float>();
      }

      private float _Clamp(float incomingValue) {
        return Mathf.Clamp(
          incomingValue,
          -(this._rotationValues.Clamp),
          this._rotationValues.Clamp
        );
      }

      [System.Serializable]
      private class _RotationValues {
        public float Clamp {
          get => this._clamp;
        }

        public float Scalar {
          get => this._scalar;
        }

        [SerializeField]
        private float _clamp  = 0;

        [SerializeField]
        private float _scalar = 1;
      }
    }
  }
}
