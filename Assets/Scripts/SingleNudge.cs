﻿using UnityEngine;

namespace RollABall {
  [AddComponentMenu("Roll A Ball/Single Nudge")]
  public class SingleNudge: MonoBehaviour
  {
    [SerializeField]
    private Rigidbody _rigidbody = null;

    private bool _didFirstNudge = false;

    private void Update() {
      if(!_didFirstNudge) {
        this._rigidbody.AddForce(
          new Vector3(
            0.0f,
            0.0f,
            0.1f
          )
        );
      }
    }
  }
}
