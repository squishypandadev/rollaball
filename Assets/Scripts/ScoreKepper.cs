﻿using TMPro;
using UnityEngine;

namespace RollABall {
  [AddComponentMenu("Roll A Ball/Score Kepper")]
  public class ScoreKepper : MonoBehaviour {
    [SerializeField]
    [NaughtyAttributes.Tag]
    private string _targetTag = "";

    [SerializeField]
    private GeneratePickup _gernatePickup = null;

    [SerializeField]
    private _Text _text = new _Text();

    private uint _score  = 0;
    private uint _scoreTarget = 0;

    private void Start() {
      this._scoreTarget = this
        ._gernatePickup
        .numberOfPickups;

      this._text.winObject.SetActive(false);

      this._UpdateScoreText();
    }

    private void Update() {
      this._UpdateScoreText();
    }

    private void OnTriggerEnter(Collider collider) {
      var isTarget = collider
        .gameObject
        .CompareTag(this._targetTag);

      if(isTarget) {
        this._score++;
      }
    }

    private void _UpdateScoreText() {
      this._text.score.text = $"Score: {this._score}";

      if(this._score == this._scoreTarget) {
        this._text.winObject.SetActive(true);
      }
    }

    [System.Serializable]
    private class _Text {
      public TextMeshProUGUI score {
        get => this._score;
      }

      public GameObject winObject {
        get => this._winObject;
      }

      [SerializeField]
      private TextMeshProUGUI _score = null;

      [SerializeField]
      private GameObject _winObject = null;
    }
  }
}
