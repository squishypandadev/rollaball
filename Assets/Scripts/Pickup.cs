﻿using UnityEngine;
using NaughtyAttributes;

namespace RollABall {
  [AddComponentMenu("Roll A Ball/Pickup")]
  public class Pickup : MonoBehaviour {
    [SerializeField]
    [Tag]
    private string _targetTag = "";

    [SerializeField]
    private AudioSource _audioSource = null;

    [SerializeField]
    private Renderer _renderer = null;

    [SerializeField]
    private BoxCollider _boxCollider = null;

    private void OnTriggerEnter(Collider collider) {
      var isTarget = collider
        .gameObject
        .CompareTag(this._targetTag);

      if(isTarget) {
        this._boxCollider.enabled = false;
        this._renderer.enabled    = false;

        this._audioSource.Play();

        Destroy(
          this.gameObject,
          this._audioSource.clip.length
        );
      }
    }
  }
}
