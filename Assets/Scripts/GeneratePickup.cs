﻿using UnityEngine;

namespace RollABall {
  [AddComponentMenu("Roll A Ball/Generate Pickup")]
  public class GeneratePickup : MonoBehaviour {
    public uint numberOfPickups {
      get => this._numberOfPickups;
    }

    [SerializeField]
    private GameObject _pickupPrefab = null;

    [SerializeField]
    private Renderer _groundRenderer = null;

    [SerializeField]
    private uint _numberOfPickups = 0;

    [SerializeField]
    private float _shrinkFactor = 1;

    private void Start() {
      var groundBounds = this
        ._groundRenderer
        .bounds
        .extents / this._shrinkFactor;

      var pickupGroup = this
        .transform
        .Find("PickupGroup")
        .gameObject;

      for(uint i = 0; i < this._numberOfPickups; i++) {
        var pickup = Instantiate(
          this._pickupPrefab,
          Vector3.zero,
          Quaternion.identity
        );

        pickup.transform.SetParent(
          pickupGroup.transform,
          false
        );

        pickup.transform.localPosition = new Vector3(
          this._GenerateRandomCordnanite(groundBounds.x),
          0,
          this._GenerateRandomCordnanite(groundBounds.z)
        );
      }
    }

    private float _GenerateRandomCordnanite(float boundValue) {
      float value =  Random.Range(
        -(boundValue),
        boundValue
      );

      return value;
    }
  }
}
